function narisi(poz, hit, pot)
% narise pot planeta in planeta

% narise pozicijo planeta in sonca
plot3(poz(1), poz(2), poz(3), "ro");
plot3(0, 0, 0, "ro");

% narise hitrost
vek = [poz poz-hit/10];
plot3(vek(1,:), vek(2,:), vek(3,:), "r", "linewidth", 5);

% narise pot
plot3(pot(1,:), pot(2,:), pot(3,:), "b");


