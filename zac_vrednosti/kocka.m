n = 10;

P = zeros(n*n*n, 7);
P(:, 4) = ones(n*n*n,1); # vpise maso

cnt = 1;
for i = 1:n
  for j = 1:n
    for k = 1:n
      P(cnt, 1) = i-1; 
      P(cnt, 2) = j-1; 
      P(cnt, 3) = k-1;
      cnt++;
    end
  end
end

clf;
axis("equal");
plot3(P(:, 1), P(:, 2), P(:, 3), "o");

csvwrite("kocka.csv", P, 'delimiter',';');