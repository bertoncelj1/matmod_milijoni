function [poz, hit, pot] = getvs(fdeg, ap, pe, inc, q)
% fdeg- position of the planet
% pe - perihelion
% ap - aphelion
% inc - inklancija orbibte [x, y, z]
% q - M * G = masa * gravitacijska konstanta -> uravnava hitrost
% 
% vrne:
% poz - pozicija
% hit - hitrost
% pot - 100 tock elipse [x; y], pot ki jo opravi delec
%
%
%

# za zacetne parametre elipse izracuna hitrost in pozicjo 
# glede na kot


fdeg = mod(fdeg+1, 360); # pozicija tocke glede na center elipse 
pe = pe;  # perihelion
ap = ap; # aphelion

# koti
kx = 0;%inc(1);
ky = inc(2);
kz = inc(3);

f = fdeg*2*pi/360; # kot v radijanih

# parametri elipse a, b, in c
a = (ap + pe)/2; 
c = (ap - pe)/2;
b = sqrt(a*a - c*c);


poz = [a*cos(f); b*sin(f); 0];


G = 6.674 * 10^-11; # gravitacijska konstanta
M = 6.417 * 10^14;  # masa planeta

# izracunana konstanta q ki jo potrebujemo za izracun hitrosti
q = q*100000;#G*M;

# fokus elipse ali v našem primeru sonce
sun = [c; 0; 0];

# pozicija tocke na elipsi
p = [a*cos(f); b*sin(f); 0];

r_sun = norm(p - sun, 2); # razdalja do sonca
vp = sqrt(q*(2/r_sun - 1/a)); # izracunana velikost hitrosi
m = -b*cos(f)/(a*sin(f)); # smer hitrosi
v = [vp/sqrt(1+m*m); vp/sqrt(1+1/(m*m)); 0]; # hitrost podana v x in y koordinatah


if 0 <= fdeg && fdeg <= 90
  v .*= [-1; 1; 0];
elseif 90 <= fdeg && fdeg <= 180
  v .*= [-1; -1; 0];
elseif 180 <= fdeg && fdeg <= 270
  v .*= [1; -1; 0];
elseif 270 <= fdeg && fdeg <= 360
  v = v.*[1; 1; 0];
end

  
# rotacijske matrike
fx = kx *2*pi/360;
fy = ky *2*pi/360;
fz = kz  *2*pi/360;
Rx = [1, 0, 0; 0 cos(fx), -sin(fx); 0, sin(fx), cos(fx)];
Ry = [cos(fy), 0, sin(fy); 0, 1, 0; -sin(fy), 0, cos(fy)];
Rz = [cos(fz), -sin(fz), 0; sin(fz), cos(fz), 0; 0, 0, 1];
R = Rx*Ry*Rz;
%R = Rx*Rz*Ry;
%R = Ry*Rx*Rz;
%R = Ry*Rz*Rx;
%R = Rz*Rx*Ry;
%R = Rz*Ry*Rx;




# vse parametre zavrtimo tako da dobimo pravilno obrnjeno elipso
p = R*p;
sun = R*sun;
v = R*v;

# ce ponovno izracunamo hitrost bi morala ostati enaka
# kar ostane in to je super! 
#r_sun = norm(p - sun, 2)
#vp = sqrt(q*(2/r_sun - 1/a))
#v
#norm(v)


# sedaj imamo vse parametre ki jih rabimo
p = p - sun; # naredimo da je sonce na sredini
poz = p; # pozicija tocke
hit = v;# hitrost tocke

% izracuna tocke na elipse 
t = linspace(-pi, pi);
pos = zeros(3, columns(t));
pos(1,:) = a*cos(t);
pos(2,:) = b*sin(t);
elipsa=R*pos;

pot = elipsa - sun;


%clf;
%hold on;
%plot3(p(1), p(2), p(3), "ro");
%plot3(sun(1), sun(2), sun(3), "ro");
%vek = [p p-v/10];
%plot3(vek(1,:), vek(2,:), vek(3,:), "r", "linewidth", 5);
%plot3(elipsa(1,:), elipsa(2,:), elipsa(3,:), "b");
%plot3(0, 0, 0, "xb");


%
% izris dobljenih opodatkov
%clf;
%hold on;
%axis("square");
%axis("equal");
%
%plot3(p(1), p(2), p(3), "ro");
%vek = [p p-v/10];
%plot3(vek(1,:), vek(2,:), vek(3,:), "r", "linewidth", 5);
%
%
%
%
%t = linspace(-pi, pi);
%pos = zeros(3, columns(t));
%pos(1,:) = a*cos(t);
%pos(2,:) = b*sin(t);
%
%pos1=R*pos;
%plot3(pos1(1,:), pos1(2,:), pos1(3,:), "b");


% Ocitno drugacni vrstni red pomeni drugacno transformacijo
%pos1=Rz*Ry*Rx*pos;
%plot3(pos1(1,:), pos1(2,:), pos1(3,:), "b");
%
%pos1=Rz*Rx*Ry*pos;
%plot3(pos1(1,:), pos1(2,:), pos1(3,:), "b");
%
%pos1=Ry*Rz*Rx*pos;
%plot3(pos1(1,:), pos1(2,:), pos1(3,:), "b");
%
%pos1=Ry*Rx*Rz*pos;
%plot3(pos1(1,:), pos1(2,:), pos1(3,:), "b");
%pos1=Rz*Ry*Rx*pos;
%plot3(pos1(1,:), pos1(2,:), pos1(3,:), "b");
%
%pos1=Rx*Rz*Ry*pos;
%plot3(pos1(1,:), pos1(2,:), pos1(3,:), "b");
%
%pos1=Rx*Ry*Rz*pos;
%plot3(pos1(1,:), pos1(2,:), pos1(3,:), "b");