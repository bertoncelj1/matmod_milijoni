function R = rot(kx, ky, kz)

fx = kx *2*pi/360;
fy = ky *2*pi/360;
fz = kz  *2*pi/360;
Rx = [1, 0, 0; 0 cos(fx), -sin(fx); 0, sin(fx), cos(fx)];
Ry = [cos(fy), 0, sin(fy); 0, 1, 0; -sin(fy), 0, cos(fy)];
Rz = [cos(fz), -sin(fz), 0; sin(fz), cos(fz), 0; 0, 0, 1];
R = Rx*Ry*Rz;