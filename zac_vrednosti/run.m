
# koti:
% AoP argument of pariapsis - ni cist res .... :(
% ecliptic plane % ce je vec kot 180 potem je retograde (v nasprotno smer)
% LoAN Longitude of ascending node 

%[poz, hit, pot] = getvs(20, 10, 60, [0, 0, 0], 0.055);
%narisi(poz, hit, pot);
%
%[poz, hit, pot] = getvs(20, 20, 100, [0, 10, 90], 0.055);
%narisi(poz, hit, pot);

##### NOTE ####
# kot je v stopinjah
# ap in pe so v giga metrih
# prvi matemeter pri inklanacija orbite se ne upošteva
# masa je glede na zemljino

#       kot ap  pe     inklanacija orbite         masa
data = [20, 69, 46,    29.124, 7, 48.33,          0.055;  # merkur
        0, 108, 107,   76.680, 3.394, 54.884,     0.815;  # venera¸
        0, 152, 147,   114.206, 0, -11.260,       1;      # zemlja
        0, 249, 206,   286.502, 1.850, 49.558,    0.531;  # mars
        0, 816, 740,   273.867, 1.303, 100.464,   317.8;  # jupiter
        0, 1509, 1350, 339.392, 2.485, 113.665,   95.159; # saturn
        0, 3008, 2742, 74.006, 0.773, 96.998,     63;     # uran
        0, 4540, 4460, 276.336, 1.767, 131.784,   17.147; # neptun 
        0, 7375, 4436, 113.834, 17.16, 110.299,   0.00218]; # pluto <3

pozicije = [];
hitrosti = [];
mase = [];
poti = zeros(3, 100, 20); 
i = 1;

# dodamo sonce
pozicije = [pozicije 0;0;0];
hitrosti = [hitrosti 0;9;0];
mase = [mase; 333000];
poti(:,:,i) = zeros(3, 100);
i++;
        
# naredi izracun in shrani podatke
for d = data'
  d(7) *= 10;

  # naredi izracun
  [poz, hit, pot] = getvs(d(1), d(2), d(3), [d(4), d(5), d(6)], d(7));
  
  # shrani podatke
  pozicije = [pozicije poz]; 
  hitrosti = [hitrosti hit]; 
  mase = [mase d(7)]; 
  poti(:,:,i) = pot; 
  i+=1;
end

podatkiCSV = [];

# shrani podetke v CSV obliko
for j = 1:i-1;

  # ena vrstica csv podatkov
  vr = [pozicije(1,j), pozicije(2,j), pozicije(3,j), mase(j), hitrosti(1,j), hitrosti(2,j), hitrosti(3,j)];
  
  # shrani v tabelo
  podatkiCSV = [podatkiCSV; vr];
end

csvwrite("osoncje.csv", podatkiCSV, 'delimiter',';');
podatkiCSV

# izris
clf;
hold on;
axis("equal");
for j = 1:i-1;
  narisi(pozicije(:,j), hitrosti(:,j), poti(:,:,j));

end



%[poz, hit, pot] = getvs(0, 200, 250, [0, 10, 3], 100000);
%narisi(poz, hit, pot);














