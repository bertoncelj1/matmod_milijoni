n = 1000;

P = zeros(n, 7);
P(:, 4) = ones(n,1); # vpise maso


cnt = 1;
for i = 1:n
  kot = (rand())*2*pi;
  kot2 = (rand())*2*pi;
  razdalja = stdnormal_rnd(1)*40;
  
  P(i, 1) = sin(kot) * cos(kot2) * razdalja;
  P(i, 2) = sin(kot) * sin(kot2) * razdalja;
  P(i, 3) = cos(kot) * razdalja;
end

# nagnjenost galaksije
R = rot(0, 0, 0);
P(:, 1:3) = P(:, 1:3)*R;

clf;
axis("equal");
plot3(P(:, 1), P(:, 2), P(:, 3), "o");

csvwrite("galaksijaOblak.csv", P, 'delimiter',';');