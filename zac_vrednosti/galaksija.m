n = 100;

P = zeros(n, 7);
P(:, 4) = ones(n,1); # vpise maso


cnt = 1;
for i = 1:n
  P(i, 4) = rand(); # vpise maso
  
  kot = (rand()-0.5)*2*pi;
  razdalja = stdnormal_rnd(1)*20;
  visina = 0;
  
  P(i, 1) = sin(kot) * razdalja;
  P(i, 2) = cos(kot) * razdalja;
  P(i, 3) = visina;

  G = 40; # konstanta
  hitrost = sqrt(P(i,4)*G/razdalja); # orbitalna hitrost
  kot_h = atan(hitrost/razdalja); # kot med tocko in hitrosjo
  raz_h = norm([razdalja; hitrost]); # razdalja do tocke hitrosti
  
  P(i, 5) = sin(kot_h + kot)*raz_h;
  P(i, 6) = cos(kot_h + kot)*raz_h;
  P(i, 7) = 0;
  
end

# nagnjenost galaksije
R = rot(0, 0, 0);
P(:, 1:3) = P(:, 1:3)*R;
P(:, 5:7) = P(:, 5:7)*R;

P(:, 5:7) = P(:, 5:7) - P(:, 1:3);



clf;
axis("equal");
plot3(P(:, 1), P(:, 2), P(:, 3), "o");

csvwrite("galaksija.csv", P, 'delimiter',';');