#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "million.h"
#include <string.h>

//prevajam z gcc -std=c99 -pedantic -Wall yugeTabela.c -lm  -o yugeTabela   

double h = 0.001; //za kak korak se premikamo
int n = 216; 		//stevilo delcev
int k = 2; 	//stevilo korakov-....cas simulacije
int dim = 2; 	//stevilo dimenzij, podpira 2 in 3 trenutno, za 1 nastav delce v linijo pa jo mas.
int grav = 1; 	//gravitacijska konstanta-> kok se prvlacjo -> moc ljubezni? -> |<3|
int sproti = 2; //po kaksnem delezu osvezujes tabelo , fino je ce ne das 0
    
int maxTableSize = 1000;
objekt *tabelaObjektov;

/**
 * Inicializira objekt z podanimi parametri
 * o - objekt ki ga nastavljamo
 * pos- pozicija objekta
 * mass - masa objekta
 * vel - hitrost objekta
 */
void initObjekt(objekt *o, vektor pos, int mass, vektor vel){
	o->position[0] = pos;
	o->position[1] = (vektor){0, 0};
	o->mass = mass;
	o->velocity = vel;
}

/**
 * Prebere eno vrstico CSV daoteke in jo razdeli na zetone
 * 
 * line - vrstica ki jo bomo pretvorili v zetone
 * tokenList - tabela v katero bojo zapisani zetoni
 */
int readCsvLine(char* line, char *tokenList[]){
    char* tok;
    int i;
    for (tok = strtok(line, ";"), i=0;
            tok && *tok;
            tok = strtok(NULL, ";\n"), i++)
    {
		tokenList[i] = tok;
    }
    return i;
}

/**
 * prebere zacetne vrednosti iz CSV datoteke
 * format datoteke: x;y;z;mass;vx;vy;vz
 * 
 * fileName- ime datoteke iz katere beremo
 * tabelaObjektov - kamor bomo zpisovali nove objekte
 * 
 * return stevilo prebranih objektov
 */
int initFromFile(objekt *tabelaObjektov, char *fileName){
	
	objekt *t = tabelaObjektov;
	
	FILE* stream = fopen(fileName, "r");
	if(!stream) {
		printf("Cannot open file '%s'!\n", fileName);
		exit(1);
	}
	
    char line[1024];
    int lineNumber = 0;
    int stObjektov = 0;
    while (fgets(line, 1024, stream))
    {
		lineNumber ++;
		if(line[0] == '#')continue; // izpustimo vrstico ki se zacne z '#'
		
		// razreti vrstico CSV na posamezne vrednosti ... zetone
		char *tokens[20];
        int numTokens = readCsvLine(line, tokens);
        
        // preverimo ali je v vrstici dovolj vrednosti
		if(numTokens < 7){
			printf("V vrstici %d, premalo zetonov: %d. Ignore\n", lineNumber, numTokens);
			continue;
		}
        
        // pretvorimo zetone v double
        double v[20]; // v kot vrednosti
        int i;
        for(i=0; i<numTokens; i++){
			double d;
			if(!sscanf(tokens[i], "%lf", &d)){
				printf("Unable to parse number: '%s' line:row %d:%d\n", tokens[i], lineNumber, i);
			}
			v[i] = d;
		}
		
		// naredimo nov objekt
		vektor poz = (vektor){v[0],v[1],v[2]};
		int masa = v[3];
		vektor hit = (vektor){v[4],v[5],v[6]};
		initObjekt(&t[stObjektov], poz, masa, hit);
		stObjektov++;
    }
    
    return stObjektov;
	
}

/**
 * tle bo nekdo nastavu default value za start 
 * ce za dvodimenzionalno poskrb da so komponente z na nuli drgac bo stala
 * 
 * n-stevilo objektov ki jih zelimo narediti
 * 
 * return stevilo narejenih objektov
 */
int prvotnaNastavitev(objekt *tabelaObjektov, int n){ 
	//objekt *t = tabelaObjektov;
	//initObjekt(t++, (vektor){ 0,  0}, 12, (vektor){ 0,   0});
	//initObjekt(t++, (vektor){-1,  0}, 2,  (vektor){ 0,  -3});
	//initObjekt(t++, (vektor){-3,  0}, 2,  (vektor){ 0, 1.44});
	
	return initFromFile(tabelaObjektov, "zac_vrednosti/osoncje.csv");
}

//printam stemle
void tmpPrint(objekt *tabelaObjektov, int n, int dim){
    //kok cele cifre
    int cela = 5;
    //kok decimalk
    int precision = 2;
    
    D printf("\n");
    for(int i = 0; i < n; i++){
        D printf("i: %2d | position: %*.*lf, %*.*lf",
        i, cela, precision, tabelaObjektov[i].position[0].x, cela, precision, tabelaObjektov[i].position[0].y);
        if(dim == 3){
            printf(", %*.*lf", cela, precision, tabelaObjektov[i].position[0].z);
        }
        D printf(" | alt position: %*.*lf, %*.*lf",
        cela, precision, tabelaObjektov[i].position[1].x, cela, precision, tabelaObjektov[i].position[1].y);
        if(dim == 3){
            printf(", %*.*lf", cela, precision, tabelaObjektov[i].position[1].z);
        }
        D printf(" | velocity: %*.*lf, %*.*lf",
        cela, precision,tabelaObjektov[i].velocity.x, cela, precision,tabelaObjektov[i].velocity.y);
        if(dim == 3){
            printf(", %*.*lf", cela, precision, tabelaObjektov[i].velocity.z);
        }
        D printf(" | force: %*.*lf, %*.*lf",
        cela, precision,tabelaObjektov[i].force.x, cela, precision,tabelaObjektov[i].force.y);
        if(dim == 3){
            printf(", %*.*lf", cela, precision, tabelaObjektov[i].force.z);
        }
        D printf(" | mass: %*.*lf\n",
        cela, precision,tabelaObjektov[i].mass);
    }
    D printf("\n");
}

//to je skupna sila vseh delcov na tega
vektor izracuniForce(objekt *tabelaObjektov, int n, int index, int grav, int alt){
    vektor tmpForce;
    tmpForce.x = 0;
    tmpForce.y = 0; 
    tmpForce.z = 0;
    
    //sprehod po tabeli delcev
    for(int i = 0; i < n; i++){
        //bedno k mormo skos cekirat, sam drgac bi delil z 0
        //mrbit bo treba tle upgradat ce 2 delca prideta en na druzga
        //k bi tehnicno tud deliu z nic(razdaljo)
        if(i != index){
            //tle not racunamo toto Newtnovo enacbo iz lista
            vektor tmpOdstevanje;
            double tmpNorm;
            vektor tmpSkalarnoM;
            vektor final;
            
            tmpOdstevanje = vektorskoOdstevanje(tabelaObjektov[i].position[alt], tabelaObjektov[index].position[alt]);
            //printf("odstevanje: %lf - %lf = %lf\n", tabelaObjektov[i].position[alt].x, tabelaObjektov[index].position[alt].x, tmpOdstevanje.x);
            //printf("odstevanje: %lf - %lf = %lf\n\n", tabelaObjektov[i].position[alt].y, tabelaObjektov[index].position[alt].y, tmpOdstevanje.y);
            tmpNorm = normNaTri(tmpOdstevanje);
            //printf("dolzinaNaTri: %lf, %lf = %lf\n\n", tmpOdstevanje.x, tmpOdstevanje.y, tmpNorm);
            double tmpZmnozek = grav * (tabelaObjektov[i].mass / tmpNorm);
            //printf("tmpZmnozek: %lf\n\n", tmpZmnozek);
            tmpSkalarnoM = skalarnoMnozenje(tmpZmnozek, tmpOdstevanje);
            //printf("mnozenje: %lf * %lf = %lf\n", tmpOdstevanje.x, tmpZmnozek, tmpSkalarnoM.x);
            //printf("mnozenje: %lf * %lf = %lf\n\n", tmpOdstevanje.y, tmpZmnozek, tmpSkalarnoM.y);
            final = vektorskoSestevanje(tmpForce, tmpSkalarnoM);
            //printf("sestevanje: %lf + %lf = %lf\n", tmpForce.x, tmpSkalarnoM.x, final.x);
            //printf("sestevanje: %lf + %lf = %lf\n\n", tmpForce.y, tmpSkalarnoM.y, final.y);
            tmpForce = final;
        }
    }
    //po returnu je treba freejat
    return tmpForce;
}

//vraca int za deljenje tabele
int pogojZaDeljenjeTabele(int n, int g, int sproti){
    double nn = (double)n;
    int rez = (int)(ceil(nn / sproti)) *(g+1);
    
    if(rez >= n){
        return n;
    }
    else{
        return rez;
    }
}

/**
 * usi kritizirajo negovo metodo k da je en bednik, tezk lajf ce si 
 * ze 300 let mrtu pa ne mors zagovarjat svojga dela
 * 
 * 
 */
int eulerjevaMetoda(objekt *tabelaObjektov, int n, double h, int k, int sproti){
    int alt;
    
    //furamo po korakih, opcijsko je lah while tle za nonstop izvajanje
    for(int i = 0; i < k; i++){
		
        //doloca ker del pozicije se updata
        alt = i % 2;
        
        //ce osvezujes tabelo bl pogosto, ce je sproti 1 je isto k ce tega nebi blo
        for(int g = 0; g < sproti; g++){
            //da bo slo hitrej se tele racuna izven notranje zanke
            int zacetniPogoj = pogojZaDeljenjeTabele(n,g-1,sproti);
            int koncniPogoj = pogojZaDeljenjeTabele(n,g,sproti);
            for(int j = zacetniPogoj; j < koncniPogoj; j++){
                vektor tmpForce = izracuniForce(tabelaObjektov, n, j, grav, alt);
                
                //printf("tmpForce na j: %d | %*.*lf, %*.*lf\n", j,13,10,tmpForce.x,13,10, tmpForce.y);
                //vektorskoSestevanje(tmpForce, );
                
                //zdej pa updatam pozicijo v alternate vectorju
                tabelaObjektov[j].position[(alt+1)%2] = vektorskoSestevanje(tabelaObjektov[j].position[alt], skalarnoMnozenje(h,tabelaObjektov[j].velocity));
                
                //updatamo hitrost 
                tabelaObjektov[j].velocity = vektorskoSestevanje(tabelaObjektov[j].velocity, skalarnoMnozenje(h, tmpForce));
                
                 
            }
        }
        //printf("\n");
    }
    return alt;
}

/**
 * usi kritizirajo negovo metodo k da je en bednik, tezk lajf ce si 
 * ze 300 let mrtu pa ne mors zagovarjat svojga dela
 * 
 * n - 
 */
int rungeKuttaMetoda(objekt *tabelaObjektov, int n, double h, int k, int dim, int grav, int sproti){
    int alt;
    
    //furamo po korakih, opcijsko je lah while tle za nonstop izvajanje
    for(int i = 0; i < k; i++){
		
        //doloca ker del pozicije se updata
        alt = i % 2;
        
        //ce osvezujes tabelo bl pogosto, ce je sproti 1 je isto k ce tega nebi blo
        for(int g = 0; g < sproti; g++){
			
            //da bo slo hitrej se tele racuna izven notranje zanke
            int zacetniPogoj = pogojZaDeljenjeTabele(n,g-1,sproti);
            int koncniPogoj = pogojZaDeljenjeTabele(n,g,sproti);
            
            for(int j = zacetniPogoj; j < koncniPogoj; j++){
                vektor tmpForce = izracuniForce(tabelaObjektov, n, j, grav, alt);
                
                //printf("tmpForce na j: %d | %*.*lf, %*.*lf\n", j,13,10,tmpForce.x,13,10, tmpForce.y);
                /*
                double k1;
                double k2;
                double k3;
                double k4;
                */
                
                //zdej pa updatam pozicijo v alternate vectorju
                tabelaObjektov[j].position[(alt+1)%2] = vektorskoSestevanje(tabelaObjektov[j].position[alt], skalarnoMnozenje(h,tabelaObjektov[j].velocity));
                
                //updatamo hitrost 
                tabelaObjektov[j].velocity = vektorskoSestevanje(tabelaObjektov[j].velocity, skalarnoMnozenje(h, tmpForce));
                
                 
            }
        }
        //printf("\n");
    }
    return alt;
}

objekt *izracunaj(int *st, int *alt){
    *alt = eulerjevaMetoda(tabelaObjektov, n, h, k, sproti);

    *st = n;
    return tabelaObjektov;
}

int main(int argc, char *argv[]){
    
    //inicializacija tabele vseh opazovanih objektov
    tabelaObjektov = malloc(maxTableSize*sizeof(objekt));

    //sam za debugganje
    //tmpPrint(tabelaObjektov, n, dim);
    
	if(argc > 1) n = initFromFile(tabelaObjektov, argv[1]);
	else n = initFromFile(tabelaObjektov, "input.csv");
    //nastavmo vrednosti iz kerih startamo
    printf("Read %d objects.\n", n);

    tmpPrint(tabelaObjektov, n, dim);
    
    // inicializira okno
    prikaz_init();
    
    //tmpPrint(tabelaObjektov, n, dim);
    
    free(tabelaObjektov);
    return 0;
}
