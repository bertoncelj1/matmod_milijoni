#ifndef MILLION_H
#define MILLION_H


// print debug messages
#define DEBUG_MODE
#define DEBUG_LEVEL 2

#ifdef DEBUG_MODE
 #define D if(1) 
 #define Dlvl(x) if(x>=DEBUG_LEVEL)
#else
 #define D if(0) 
 #define Dlvl(x) if(0) 
#endif




typedef struct vektor{
  double x;
  double y;
  double z;
} vektor;

typedef struct objekt{
  vektor position[2];
  vektor velocity;
  vektor force;
  double mass;
} objekt;

#define vektorskoOdstevanje(a, b)   ((vektor){a.x-b.x, a.y-b.y, a.z-b.z})
#define vektorskoSestevanje(a, b)   ((vektor){a.x+b.x, a.y+b.y, a.z+b.z})
#define skalarnoMnozenje(b, a)      ((vektor){a.x * b, a.y * b, a.z * b})
#define kvadratNorme(a)             (pow(a.x,2) + pow(a.y,2) + pow(a.z,2))
#define norm(a)                     (sqrt(kvadratNorme(a)))
#define normNaTri(a)                (pow(norm(a), 3))

void prikaz_init();
objekt *izracunaj(int *st, int *alt);

#endif
