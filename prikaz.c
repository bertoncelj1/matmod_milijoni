#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include "million.h"

int Width, Height;
int n, alt;
objekt *tabela;
int point = 0;
int pause = 1;

#define UPDATE_INTERVAL     30 // ms

GLfloat light_white[] = {1.0, 1.0, 1.0, 1.0};  /* Diffuse light. */
GLfloat light_gray[] = {0.3, 0.3, 0.3, 1.0};  /* Diffuse light. */
GLfloat light_position[] = {1.0, 1.0, 1.0, 0.0};  /* Light location. */

GLfloat normals[6][3] = {  /* Normals for the 6 faces of a cube. */
  {-1.0, 0.0, 0.0}, {0.0, 1.0, 0.0}, {1.0, 0.0, 0.0},
  {0.0, -1.0, 0.0}, {0.0, 0.0, 1.0}, {0.0, 0.0, -1.0} };
GLint indices[6][4] = {  /* Vertex indices for the 6 faces of a cube. */
  {0, 1, 2, 3}, {3, 2, 6, 7}, {7, 6, 5, 4},
  {4, 5, 1, 0}, {5, 6, 2, 1}, {7, 4, 0, 3} };
GLfloat v[8][3];  /* Will be filled in with X,Y,Z vertexes. */

GLfloat eye[] = {0., 0., 20.};

GLuint buffer;

void display(){
    clock_t begin = clock();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    
    if(tabela != NULL){
		
        if(!point){
			glVertexPointer(3, GL_FLOAT, 0, &(v[0][0]));
        	glEnableClientState(GL_VERTEX_ARRAY);

        	glNormalPointer(GL_FLOAT, 0, normals);
        	glEnableClientState(GL_NORMAL_ARRAY);

			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer);	
		}
        for(int i = 0; i < n; i++){
            
            // barvo dolocimo glede na hitrost objekta
            double hitrost = log(norm(tabela[i].velocity));
			vektor p = tabela[i].position[alt];
			if(pause) glColor3f(1, 1, 1);
            else glColor3f(hitrost, 1.0-(hitrost), 0);
				glLoadIdentity();
			
				gluLookAt(eye[0], eye[1], eye[2],  /* eye */
						eye[0], eye[1], eye[2]-10.,      /* center*/
						0.0, 1.0, 0.);      /* up is in positive Y direction */
            if(point){// izrise objekt (tocko)
				glPointSize(5.);
	            glBegin(GL_POINTS);
	            glVertex3f(p.x, p.y, p.z);
				glEnd();
			}
			else{
				
				glTranslatef(p.x, p.y, p.z);
				float scale = 0.05*cbrt(tabela[i].mass);
				glScalef(scale, scale, scale);
		        glDrawElements(GL_QUADS, 24, GL_UNSIGNED_INT, 0);
			}
            Dlvl(1) printf("%d: %f %f\n", i, p.x, p.y);
        }
		if(!point) glBindBuffer(GL_ARRAY_BUFFER, 0);
        Dlvl(1) printf("---\n");
    }
    
    glutSwapBuffers();
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    Dlvl(2) printf("Draw: %lf ms\n", time_spent*1000.);
}

/**
 * Glavn zanka izračuna
 **/
void update(){
    clock_t begin = clock();
    
    if(!pause || tabela == NULL) tabela = izracunaj(&n, &alt);
	glutPostRedisplay();
    glutTimerFunc(UPDATE_INTERVAL, update, 0);
	
    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    Dlvl(2) printf("Update: %lf ms\n", time_spent*1000.);
}

void addElementBuffer(GLuint *destination, GLuint *data, int elements){
	glGenBuffers(1, destination);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, *destination);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, elements*sizeof(GLuint), data, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void mouse(int x, int y){
	
}

void keyboard(unsigned char key, int x, int y){
    switch(key){
        case 'w': eye[1]+=0.1; break;
        case 's': eye[1]-=0.1; break;
        case 'a': eye[0]-=0.1; break;
        case 'd': eye[0]+=0.1; break;
		case 'r': eye[2]-=0.1; break;
		case 'f': eye[2]+=0.2; break;
		case 'p': point = !point; break;
        case 'q': exit(0); break;
		case ' ': pause = !pause; break;
    }
}

/**
 * Inicializira okno
 */
void prikaz_init()
{
	/* Setup cube vertex data. */
    v[0][0] = v[1][0] = v[2][0] = v[3][0] = -1;
    v[4][0] = v[5][0] = v[6][0] = v[7][0] = 1;
    v[0][1] = v[1][1] = v[4][1] = v[5][1] = -1;
    v[2][1] = v[3][1] = v[6][1] = v[7][1] = 1;
    v[0][2] = v[3][2] = v[4][2] = v[7][2] = 1;
    v[1][2] = v[2][2] = v[5][2] = v[6][2] = -1;

    tabela = NULL;
    Width = 800;
    Height = 600;
    char *myargv [1];
    int myargc=1;
    myargv [0] = ("milijon");
    glutInit(&myargc, myargv);
    glutInitWindowSize(Width, Height);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutCreateWindow("Milijon");
    glutDisplayFunc(display);
    glutTimerFunc(UPDATE_INTERVAL, update, 0);
	glutKeyboardFunc(keyboard);

    /* Enable a single OpenGL light. */
	/*glLightfv(GL_LIGHT0, GL_DIFFUSE, light_white);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);*/

	/* Use depth buffering for hidden surface elimination. */
	glEnable(GL_DEPTH_TEST);
    //glEnable(GL_POINT_SMOOTH);
	glEnable(GL_CULL_FACE);

	glClearColor(2/255.0, 20/255.0, 2/255.0, 1.0);

	/* Setup the view of the cube. */
	glViewport(0, 0, Width, Height);
	glMatrixMode(GL_PROJECTION);
	gluPerspective( /* field of view in degree */ 70.0,
		/* aspect ratio */ (float)Width/(float)Height,
		/* Z near */ 0.1, /* Z far */ 208208.0);
	glMatrixMode(GL_MODELVIEW);
	gluLookAt(5., 2., 3.,  /* eye */
		0.0, 0.0, 0.0,      /* center*/
		0.0, 1.0, 0.);      /* up is in positive Y direction */
	addElementBuffer(&buffer, &(indices[0][0]), 24);
    Dlvl(2) printf("OpenGL inicializiran\n");
    glutMainLoop();
	exit(0);
}
